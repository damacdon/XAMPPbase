# this file installs Julia (https://julialang.org/)
# just include it into your favourite CMakeLists

include(${CMAKE_ROOT}/Modules/ExternalProject.cmake)
set_directory_properties(PROPERTIES EP_BASE "${CMAKE_BINARY_DIR}/Externals/")

# define variable with build path
set( _JuliaBuildDir "${CMAKE_BINARY_DIR}/Externals/Source/Julia" )

# add Julia as external project
ExternalProject_Add(Julia
    GIT_REPOSITORY https://github.com/JuliaLang/julia
    GIT_TAG v1.0.0
    # the installation only requires the 'make' step, thus no 'configure' or 'make install' commands called
    CONFIGURE_COMMAND ""
    INSTALL_COMMAND ""
    #
    SOURCE_DIR "${_JuliaBuildDir}"
    BINARY_DIR "${_JuliaBuildDir}"
    # since the checkout from git takes a while, better show progress bar
    GIT_PROGRESS 1
)

# link the Julia executable to 'julia' in order to execute it later
ExternalProject_Add_Step( Julia executable
    COMMAND ln -f -s ${_JuliaBuildDir}/julia ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/julia 
    COMMENT "Create shared symlink"
    DEPENDEES build 
)
   
# unset build path variable
unset( ${_JuliaBuildDir} )

